<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function addCategory(Request $request){
        $category = new Category();
        $category->name = $request->input('category');
        $category->save();
        return json_encode('saved');
    }
}
