$(function(){
	$(".modal").on("hidden.bs.modal", function(){
		$(this).removeData();
	});

	url = window.location.href.split('/').pop();
	$(".sidebar-menu a[href='"+url+"']").parent().addClass('active');

	$('table input[type=checkbox]').on('ifChanged', function(){
		if ($("table input[type=checkbox]:checked").size()>0){
			$('#btn-new-invoice').hide();
			$('#btn-remind').hide();
			$('#check-tools').show();
		}
		else{
			$('#btn-new-invoice').show();
			$('#btn-remind').show();
			$('#check-tools').hide();
		}

	});

	$('table .row-checkbox input[type=checkbox]').on('ifChanged', function(){
		if ($(this).is(':checked'))
			$(this).parents('table').find('input[type=checkbox]').iCheck('check');
		else
			$(this).parents('table').find('input[type=checkbox]').iCheck('uncheck');
	});

	$('#send-reminders-automatically').on('ifChanged', function(){
		console.log('x');
		if ($(this).is(':checked'))
			$(".days-of-week table").show();
		else
			$(".days-of-week table").hide();
	});


	$('table.selectable tbody tr').click(function(){

		//$('table.selectable tbody tr').removeClass('selected');
		//$(this).addClass('selected');


		//$(this).find('input[type=checkbox]').iCheck('toggle');
	});

	$(document).on('click', '#tabs-menu a', function(e){
		e.preventDefault();
		$('#tabs>div').hide();
		$('#tabs '+$(this).attr('href')).show();
		$('#tabs-menu li').removeClass('active');
		$(this).parents('li').addClass('active');
	});

	$('.slimscroll').slimScroll({
		height: '767px',
		wheelStep: '5px'
	});

	$('.item-price').on('focus', function(){
		$(this).siblings('.btn-discount').show();
	});

	$('.btn-discount').on('click', function(e){
		e.preventDefault();
		$(this).siblings('.discount').show();
		$(this).addClass("active");
	});

	$('.select2').select2();


	$('.redactor').each(function(index){
		height = parseInt($(this).attr('data-height')) || 250;
		$(this).redactor({
			buttons: ['bold', 'italic', 'underline', 'lists', 'link'],
			replaceDivs: false,
			paragraphize: false,
			maxHeight: height
		});
	});


	$('input.icheck').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_square',
		increaseArea: '70%' // optional
	});


	$('.reminder-flow a').click(function(e){

		$('.flow-selected').show();
		$('.flow-not-selected').hide();

		$('.reminder-flow li').removeClass('active');
		$(this).parents('li').addClass('active');
		e.preventDefault();
	});

	$('.category-list li a').click(function(){
		$('.category-list li').removeClass('active');
		$(this).parents('li').addClass('active');
	});

	$('#accent-color li').each(function(){
		$(this).css({'backgroundColor':$(this).attr('data-color')});
	});

});
