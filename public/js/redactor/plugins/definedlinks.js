if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.definedlinks = function()
	{
		return {
			init: function()
			{
				if (!this.opts.definedLinks) return;

				this.modal.addCallback('link', $.proxy(this.definedlinks.load, this));
				
			},
			
			load: function()
			{				
				var $modal = this.modal.getModal();
				
				var $internal = $('<div id="redactor-modal-link-internal" class="redactor-tab redactor-tab1">');
				var $external = $('#redactor-modal-link-insert').wrapInner('<div id="redactor-modal-link-external" class="redactor-tab redactor-tab2">');
				var $advanced = $('<div id="redactor-modal-link-advanced" class="redactor-tab redactor-tab3">');
				
				$('#redactor-modal-link-insert').append($internal);
				$('#redactor-modal-link-insert').append($advanced);
				$('#redactor-modal-link-external').hide();
				$('#redactor-modal-link-advanced').hide();
				
				this.modal.createTabber($modal);
				this.modal.addTab(1, 'Interne link', 'active');
				this.modal.addTab(2, 'Externe link');
				this.modal.addTab(3, 'Geavanceerd');
				
				// Internal links
				$menu_list = $('<ul class="link-picker"></ul>');
				$internal.append($menu_list);
				
				this.link.getData();
				
				
				$.getJSON(this.opts.definedLinks, $.proxy(function(data)
				{
					this.definedlinks.add_links(data[0].items, $menu_list);
				}, this))
				.done($.proxy(function()
				{
					$internal.find('ul a').on('click', $.proxy(this.definedlinks.select, this));
					$internal.find('.link-picker a[data-url="'+this.link.url+'"]').addClass('active');
				}, this));
				
				
				// advanced
				$advanced.append($('<label>CSS</label><input type="text" name="css" id="redactor-link-css">'));


			},
			
			add_links: function(children, p){
				$.each(children, $.proxy(function(key, child)
				{
					li = $('<li>');
					li.append($('<a>').attr('data-url', child.workname).html(child.txt));
					p.append(li);
					
					if (child.items){
						pa = $('<ul>');
						li.append(pa);
						this.definedlinks.add_links(child.items, pa);
					}

				}, this));
			},
			
			select: function(e)
			{
				$('#redactor-link-url').val($(e.target).attr('data-url'));
				if ($('#redactor-link-url-text').val()==='') $('#redactor-link-url-text').val($(e.target).html());
				
				
				$('.link-picker').find('.active').removeClass('active');
				$(e.target).addClass('active');
			}
		};
	};
})(jQuery);