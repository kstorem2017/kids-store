"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_component_1 = require("./components/app.component");
var home_component_1 = require("./components/home.component");
var app_routes_1 = require("./app.routes");
require("./rxjs-extensions");
var base_service_1 = require("./services/base.service");
var products_component_1 = require("./components/products.component");
var header_component_1 = require("./components/header.component");
var show_product_component_1 = require("./components/show-product.component");
var cart_component_1 = require("./components/cart.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(app_routes_1.routes, { useHash: true }),
            http_1.JsonpModule
        ],
        declarations: [
            app_component_1.AppComponent,
            home_component_1.HomeComponent,
            products_component_1.ProductsComponent,
            header_component_1.HeaderComponent,
            show_product_component_1.ShowProductComponent,
            cart_component_1.CartComponent
        ],
        bootstrap: [app_component_1.AppComponent],
        providers: [
            base_service_1.BaseService
        ]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map