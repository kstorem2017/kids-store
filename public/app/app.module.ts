import {NgModule}                   from '@angular/core';
import {BrowserModule}              from '@angular/platform-browser';
import {FormsModule}                from '@angular/forms';
import {HttpModule, JsonpModule}    from '@angular/http';
import {RouterModule}               from '@angular/router';

import {AppComponent}              from "./components/app.component";
import {HomeComponent}              from "./components/home.component";
import {routes}                     from "./app.routes";
import './rxjs-extensions';
import {BaseService} from "./services/base.service";
import {ProductsComponent} from "./components/products.component";
import {HeaderComponent} from "./components/header.component";
import {ShowProductComponent} from "./components/show-product.component";
import {CartComponent} from "./components/cart.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes, { useHash: true }),
        JsonpModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        ProductsComponent,
        HeaderComponent,
        ShowProductComponent,
        CartComponent
    ],
    bootstrap: [ AppComponent ],
    providers: [
        BaseService
    ]
})
export class AppModule {}