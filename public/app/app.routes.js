"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var home_component_1 = require("./components/home.component");
var products_component_1 = require("./components/products.component");
var show_product_component_1 = require("./components/show-product.component");
var cart_component_1 = require("./components/cart.component");
// Route Configuration
exports.routes = [
    { path: 'products', component: products_component_1.ProductsComponent },
    { path: 'products/show', component: show_product_component_1.ShowProductComponent },
    { path: 'cart', component: cart_component_1.CartComponent },
    { path: '', component: home_component_1.HomeComponent }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routes.js.map