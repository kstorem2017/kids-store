import {ModuleWithProviders}        from '@angular/core';
import {Routes, RouterModule}       from '@angular/router';
import {HomeComponent}              from "./components/home.component";
import {ProductsComponent} from "./components/products.component";
import {ShowProductComponent} from "./components/show-product.component";
import {CartComponent} from "./components/cart.component";

// Route Configuration
export const routes: Routes = [
    { path: 'products',   component: ProductsComponent},
    { path: 'products/show',   component: ShowProductComponent},
    { path: 'cart',   component: CartComponent},
    { path: '',   component: HomeComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);