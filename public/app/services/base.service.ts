import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';

@Injectable()

export class BaseService{
    constructor(private http:Http){ }

    getTest(){
        return this.http.get('test').map((res) => res.json());
    }
}