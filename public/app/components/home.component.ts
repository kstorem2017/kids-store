import {Component} from '@angular/core';
import {BaseService} from "../services/base.service";

@Component({
    templateUrl: '../templates/home.component.html'
})

export class HomeComponent{
    constructor(private baseService:BaseService){
        this.baseService.getTest().subscribe(data => {
            console.log(data);
        })
    }
}