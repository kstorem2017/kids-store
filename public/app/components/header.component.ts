import {Component, AfterViewInit} from '@angular/core';
import {BaseService} from "../services/base.service";
declare var $:any;

@Component({
    selector:'header-app',
    templateUrl: '../templates/header.component.html'
})

export class HeaderComponent implements AfterViewInit{
    constructor(private baseService:BaseService){

    }

    ngAfterViewInit(): void {
        $(".megamenu").megamenu();
    }
}