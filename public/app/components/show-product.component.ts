import {Component} from '@angular/core';
import {BaseService} from "../services/base.service";

@Component({
    templateUrl: '../templates/show-product.component.html'
})

export class ShowProductComponent{
    constructor(private baseService:BaseService){

    }
}