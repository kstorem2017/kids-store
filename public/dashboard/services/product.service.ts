import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';

@Injectable()

export class ProductService{
    constructor(private http:Http){ }

    getTest(){
        return this.http.get('test').map((res) => res.json());
    }

    addCategory(name:string){
        return this.http.post('/add_category',{category:name})
            .map((res) => res.json());
    }
}