import {NgModule}                   from '@angular/core';
import {BrowserModule}              from '@angular/platform-browser';
import {FormsModule}                from '@angular/forms';
import {HttpModule, JsonpModule}    from '@angular/http';
import {RouterModule}               from '@angular/router';
import './rxjs-extensions';
import {routes}                     from "./app.routes";
import {AppComponent}              from "./components/app.component";
import {HomeComponent}              from "./components/home.component";
import {HeaderComponent} from "./components/header.component";
import {ProductComponent} from "./components/product.component";
import {SubcategoryComponent} from "./components/subcategory.component";
import {AddProductComponent} from "./components/add-product.component";
import {ProductService} from "./services/product.service";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes, { useHash: true }),
        JsonpModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        AddProductComponent,
        ProductComponent,
        SubcategoryComponent
    ],
    bootstrap: [ AppComponent ],
    providers: [
        ProductService
    ]
})
export class AppModule {}