import {ModuleWithProviders}        from '@angular/core';
import {Routes, RouterModule}       from '@angular/router';
import {HomeComponent}              from "./components/home.component";
import {ProductComponent} from "./components/product.component";
import {SubcategoryComponent} from "./components/subcategory.component";
import {AddProductComponent} from "./components/add-product.component";

// Route Configuration
export const routes: Routes = [
    { path: 'add',   component: AddProductComponent},
    { path: 'products',   component: ProductComponent},
    { path: 'subcategory',   component: SubcategoryComponent},
    { path: '',   component: HomeComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);