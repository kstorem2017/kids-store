"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
require("./rxjs-extensions");
var app_routes_1 = require("./app.routes");
var app_component_1 = require("./components/app.component");
var home_component_1 = require("./components/home.component");
var header_component_1 = require("./components/header.component");
var product_component_1 = require("./components/product.component");
var subcategory_component_1 = require("./components/subcategory.component");
var add_product_component_1 = require("./components/add-product.component");
var product_service_1 = require("./services/product.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(app_routes_1.routes, { useHash: true }),
            http_1.JsonpModule
        ],
        declarations: [
            app_component_1.AppComponent,
            header_component_1.HeaderComponent,
            home_component_1.HomeComponent,
            add_product_component_1.AddProductComponent,
            product_component_1.ProductComponent,
            subcategory_component_1.SubcategoryComponent
        ],
        bootstrap: [app_component_1.AppComponent],
        providers: [
            product_service_1.ProductService
        ]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map