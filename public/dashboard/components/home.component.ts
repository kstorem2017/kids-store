import {Component} from '@angular/core';
import {ProductService} from "../services/product.service";

@Component({
    moduleId:module.id,
    templateUrl: '../dash-templates/home.component.html'
})

export class HomeComponent{
    public categoryName = '';
    constructor(private productService:ProductService){}

    addCategory(){
        this.productService.addCategory(this.categoryName).subscribe(data => {
            console.log(data);
        })
    }

    test(){
        // console.log(this.categoryName);
    }
}