import {Component} from '@angular/core';

@Component({
    selector:'main-app',
    template: '<header-app></header-app><router-outlet></router-outlet>'
})

export class AppComponent{

}