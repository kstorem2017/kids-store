"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var home_component_1 = require("./components/home.component");
var product_component_1 = require("./components/product.component");
var subcategory_component_1 = require("./components/subcategory.component");
var add_product_component_1 = require("./components/add-product.component");
// Route Configuration
exports.routes = [
    { path: 'add', component: add_product_component_1.AddProductComponent },
    { path: 'products', component: product_component_1.ProductComponent },
    { path: 'subcategory', component: subcategory_component_1.SubcategoryComponent },
    { path: '', component: home_component_1.HomeComponent }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routes.js.map